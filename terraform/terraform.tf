terraform {

  backend "remote" {
    organization = "TERRAFORM_ORG"

    workspaces {
      prefix = "WORKSPACE_PREFIX"
    }
  }

  required_providers {
    cpln = {
      version = "1.0.0"
      source  = "controlplane.com/com/cpln"
    }
  }
}

# Variable is obtained from the 'TF_VAR_org` environment variable
variable "org" {
  type    = string
  default = ""
}

# Variable is obtained from the 'TF_VAR_token` environment variable
variable "token" {
  type    = string
  default = ""
}

# Variable is obtained from the 'TF_VAR_image` environment variable in the format IMAGE_NAME:TAG
variable "image" {
  type    = string
  default = ""
}

# Variable is obtained from the 'TF_VAR_gvc` environment variable
variable "gvc" {
  type    = string
  default = ""
}

# Variable is obtained from the 'TF_VAR_workload` environment variable
variable "workload" {
  type    = string
  default = ""
}

provider "cpln" {
  org   = var.org
  token = var.token
}

resource "cpln_gvc" "gvc" {

  name        = var.gvc
  description = "GVC created by Terraform"

  # Available locations: aws-eu-central-1, aws-us-west-2, azure-eastus2, gcp-us-east1
  locations = ["gcp-us-east1"]
}

resource "cpln_workload" "workload" {

  gvc = cpln_gvc.gvc.name

  name        = var.workload
  description = "Workload created by Terraform - Updated at 3:07pm"

  container {
    name  = "app-image"
    image = "/org/${var.org}/image/${var.image}"
    port  = 8080
  }

  options {
    capacity_ai     = false
    timeout_seconds = 5

    autoscaling {
      metric          = "concurrency"
      target          = 100
      max_scale       = 1
      min_scale       = 1
      max_concurrency = 500
    }
  }

  firewall_spec {
    external {
      inbound_allow_cidr = ["0.0.0.0/0"]
    }
  }
}
