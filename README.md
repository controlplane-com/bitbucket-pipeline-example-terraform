# Control Plane - Bitbucket Pipeline Example Using Terraform

This example demonstrates building and deploying an app to Control Plane using Terraform as part of a CI/CD pipeline. 

The example is a Node.js app that displays the environment variables and start-up arguments.

Terraform requires the current state be persisted between deployments. This example uses [Terraform Cloud](https://app.terraform.io/) to manage the state.

This example is provided as a starting point and your own unique delivery and/or deployment requirements will dictate the steps needed in your situation.

## Terraform Cloud Set Up

Follow these instructions to set up an account and workspace at Terraform Cloud:

1. Create an account at [https://app.terraform.io/](https://app.terraform.io/).

2. Create an organization. Keep a note of the organization. It will be used in the example set up section.

3. Create a workspace. Select the `CLI-driven workflow` type. The example uses a `workspace prefix` when configuring the workspace block in the `/terraform/terraform.tf` file. This allows a Terraform workspace to be used for each branch deployed. The prefix is prepended to the value of the environment variable `TF_WORKSPACE` when Terraform is executing. When creating the workspace in the wizard, the name should be the prefix followed by the branch name. For example, `cpln-dev` and `cpln-main` (prefix is `cpln-`). Keep a note of the workspace. It will be used in the example set up section.

4. Set the execution mode to `local` in the general settings. 
   - Click the `Settings` button in the top menu, then click the `General` button.
   - Click the `Local` radio button, then click the `Save Settings` button at the bottom of the page.

5. Create an API token:
    - Click on your user profile (upper right corner) and click `User settings`.
    - Click Tokens on the left side menu.
    - Click `Create an API token`, enter a description, and click `Create API token`.
    - Keep a note of the token. It will be used in the example set up section.

## Control Plane Authentication Set Up 

The Terraform provider and Control Plane CLI require a `Service Account` with the proper permissions to perform actions against the Control Plane API. 

1. Follow the Control Plane documentation to create a Service Account and create a key. Take a note of the key. It will be used in the next section.
2. Add the Service Account to the `superusers` group. Once the GitHub Actions executes as expected, a policy can be created with a limited set of permissions and the Service Account can be removed from the `superusers` group.
  
## Example Set Up

When triggered, the pipeline will generate a Terraform plan based on the HCL in the `/terraform/terraform.tf` file. The example will create/update a GVC and workload hosted at Control Plane. After the plan has been reviewed by the user, the `apply` step can be triggered which will containerize and push the application to the org's private image repository and apply the Terraform plan. Any changes to the HCL and/or application committed to the repository will trigger the pipeline. After the apply step is completed, the updated image is deployed to Control Plane.

**Perform the following steps to set up the example:**

1. Fork the example into your own workspace.
   
2. The following variables are required and must be added as repository variables. Bitbucket refers to environment variables as repository variables.
   
    Browse to the variables page by:

    - Clicking `Repository settings` (left menu bar), then 
    - Click `Repository variables` (under the pipelines section).
  
    Add the following variables:
    
    - `TF_VERSION`: Terraform CLI version to install (current version is 1.0.9).
    - `CPLN_TF_PROVIDER_VERSION`: Control Plane Terraform Provider version to install (current version is 1.0.1).
    - `CPLN_ORG`: Control Plane org.
    - `CPLN_TOKEN`: Service Account Key (SECURED).
    - `CPLN_IMAGE_NAME`: The name of the image that will be deployed. The pipeline will append the short SHA of the commit as the tag when pushing the image to the org's private image repository.
    - `TF_CLOUD_TOKEN`: Terraform Cloud Token (SECURED).
    - `TF_WORKSPACE`:  Terraform workspace name without the prefix.
    - `TF_VAR_gvc`: GVC name used by the example Terraform script. 
    - `TF_VAR_workload`: Workload name used by the example Terraform script. 

3. Review the `bitbucket-pipeline.yml` file:
    - This file contains the commands that Bitbucket will execute when the pipeline is triggered. 
    - The common script sets up the dependencies needed for each step since Bitbucket runs each step in its own container. 
    - The pipeline can be updated to be triggered on specific branch actions (pushes, pull requests, etc.) and environments. 
    - The example is set to the `default` trigger which will trigger on any change to any branch. The apply step can only be triggered manually after the successful execution of the plan step.

4. Update the Terraform HCL file located at `/terraform/terraform.tf` using the values that were created in the `Terraform Cloud Set Up` section:
    - `TERRAFORM_ORG`: The Terraform Cloud organization.
    - `WORKSPACE_PREFIX`: The Terraform Workspace Prefix. Only enter the prefix. Terraform will automatically append the value of the `TF_WORKLOAD` environment variable when pushing the state to the Terraform cloud. This comes in handy when deploying to multiple branches/environments. Each branch/environment can have the `TF_WORKLOAD` variable modified to point to its own workspace in the Terraform cloud. 

5. The file `/terraform/.terraformrc` must be included to allow Terraform to authenticate to their cloud service. No modification is necessary. The pipeline will update the credentials during execution with the value of the `TF_CLOUD_TOKEN` environment variable.

6. Run the pipeline by:
    - Clicking `Pipelines` in the left menu.
    - Click `Enable pipeline` and the current pipeline will execute.
    - Click on the pipeline link that executed. Click the `Run` button.
  

## Running the App

After the pipeline has successfully deployed the application, it can be tested by following these steps:

1. Browse to the Control Plane Console.
2. Select the GVC that was set in the `TF_VAR_gvc` variable.
3. Select the workload that was set in the `TF_VAR_workload` variable.
4. Click the `Open` button. The app will open in a new tab. The container's environment variables and start up arguments will be displayed.

## Upgrading Provider Version

1. Update the `CPLN_TF_PROVIDER_VERSION` repository variable with the desired provider version.
2. Update the version property inside the HCL file that contains the `required_providers` declaration block for the `cpln` provider. 
3. The example pipeline runs the command `terraform init -upgrade` which will upgrade the Terraform dependencies (state file, etc.).

Note: If necessary, the provider version can be downgraded.

## Helper Links

Terraform

- [Terraform Documentation](https://www.terraform.io/docs/index.html)
  
- [Terraform Cloud Credentials](https://www.terraform.io/docs/cli/config/config-file.html)


Bitbucket

- [Configure Bitbucket Pipelines](https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/)

- [Bitbucket Variables and Secrets](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/)

- [Pipeline YAML Validator](https://bitbucket-pipelines.prod.public.atl-paas.net/validator)